drop database if exists company;
create database company;
use company;
SET SQL_SAFE_UPDATES=0;
create table department (
dname char(20),
dnumber char(1) primary key,
mgrssn char(9),
mgrstartdate char(6),
unique (dname));

create table employee (
ssn char(9) primary key,
first_name char(15),
last_name char(15),
bdate char(6),
sex char(1) not null,
salary int,
superssn char(9) references employee(ssn),
dno char(1) references department(dnumber));


create table dept_locations (
dnumber char(1),
dlocation char(10),
primary key (dnumber, dlocation));

create table project (
pname char(20),
pnumber char(2) primary key,
plocation char(10),
dnum char(1) references department(dnumber));

create table works_on (
essn char(9) references employee(ssn),
pno char(2) references project(pnumber),
hours int,
primary key (essn, pno));

insert employee values(123456789,"JOHN","SMITH",010955,"M",30000,333445555,5);
insert employee values(333445555,"FRANKLIN","WONG",120845,"M",40000,888665555,5);
insert employee values(999887777,"ALICIA","ZELAYA",071958,"F",25000,987654321,4);
insert employee values(987654321,"JENNIFER","WALLACE",062031,"F",43000,888665555,4);
insert employee values(666884444,"RAMESH","NARAYAN",091552,"M",38000,333445555,5);
insert employee values(453453453,"JOYCE","ENGLISH",063162,"F",25000,333445555,5);
insert employee values(987987987,"AHMAD","JABBAR",032959,"M",25000,987654321,4);
insert employee values(888665555,"JAMES","BORG",111027,"M",55000,null,1);


insert department values ("RESEARCH",5,333445555,052278);
insert department values ("ADMINISTRATION",4,987654321,010185);
insert department values ("HEADQUARTERS",1,888665555,061971);


insert dept_locations values(1,"HOUSTON");
insert dept_locations values(4,"STAFFORD");
insert dept_locations values(5,"BELLAIRE");
insert dept_locations values(5,"SUGARLAND");
insert dept_locations values(5,"HOUSTON");

insert works_on values (123456789,1,32.5);
insert works_on values (123456789,2,7.5);
insert works_on values (666884444,3,40.0);
insert works_on values (453453453,1,20.0);
insert works_on values (453453453,2,20.0);
insert works_on values (333445555,2,10.0);
insert works_on values (333445555,3,10.0);
insert works_on values (333445555,10,10.0);
insert works_on values (333445555,20,10.0);
insert works_on values (999887777,30,30.0);
insert works_on values (999887777,10,10.0);
insert works_on values (987987987,10,35.0);
insert works_on values (987987987,30,5.0);
insert works_on values (987654321,30,20.0);
insert works_on values (987654321,20,15.0);
insert works_on values (888665555,20,null);

insert project values ("PRODUCTX",1,"BELLAIRE",5);
insert project values ("PRODUCTY",2,"SUGARLAND",5);
insert project values ("PRODUCTZ",3,"HOUSTON",5);
insert project values ("COMPUTERIZATION",10,"STAFFORD",4);
insert project values ("REORGANIZATION",20,"HOUSTON",1);
insert project values ("NEWBENEFITS",30,"STAFFORD",4);