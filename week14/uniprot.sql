#inital time 0.656
#after index 0.344
#after PK 0.094`PRIMARY`


use mydb;
delete from proteins;
select * from proteins;

load data local infile 'C:\\Users\\furka\\Downloads\\Compressed\\insert\\insert.txt' into table proteins fields terminated by '|';

select *
from proteins 
where protein_name like "%tumor%" and uniprot_id like "%human%"
order by uniprot_id;

create index uniprot_index on proteins (uniprot_id);
drop index uniprot_index on proteins;

alter table proteins add constraint pk_proteins primary key (uniprot_id);